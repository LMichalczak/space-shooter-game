﻿using UnityEngine;

public class DestroyByCollision : MonoBehaviour
{
    private GameController gameController;
    public int scoreForOneKill;
   // public GameObject explosion;
    public GameObject playerExplosion;

    void Start()
    {
        GameObject gameControllerGameObject = GameObject.FindWithTag("GameController");
        if (gameControllerGameObject != null)
            gameController = gameControllerGameObject.GetComponent<GameController>();
    }

    protected virtual bool OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BattleGround")  || other.CompareTag("Enemy") || other.CompareTag("Fuel"))
            return false;
        
       // if(explosion != null) 
         //   Instantiate(explosion, transform.position, transform.rotation);

        if (other.CompareTag("Player"))
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }

        gameController.AddScore(scoreForOneKill);
        Destroy(other.gameObject);
        Destroy(gameObject);
        return true;
    }
}
