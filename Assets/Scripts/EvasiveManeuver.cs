﻿using System.Collections;
using UnityEngine;

public class EvasiveManeuver : MonoBehaviour
{
    public PositionXZLimits positionXZLimits;
    public float dodge;
    public float smoothing;
    public float tilt;
    public Vector2 startWait;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;

    private float currentSpeed;
    private float targetManeuver;
    private Rigidbody rb;
    void Start ()
	{
	    rb = GetComponent<Rigidbody>();
	    currentSpeed = rb.velocity.z;
	    StartCoroutine(Evade());
	}
	
	void FixedUpdate ()
	{
	    SetObjectVelocity();
        limitObjectPosition();
	    rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
    }
    /// <summary>
    /// Set velocity of object
    /// </summary>
    void SetObjectVelocity()
    {
        float newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime * smoothing);
        rb.velocity = new Vector3(newManeuver, 0.0f, currentSpeed);
    }

    /// <summary>
    /// Limit position of object on x and z axis
    /// </summary
    void limitObjectPosition()
    {
        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, positionXZLimits.xMin, positionXZLimits.xMax),
            0.0f,
            Mathf.Clamp(rb.position.z, positionXZLimits.zMin, positionXZLimits.zMax));
    }

    /// <summary>
    /// Maka evades after delay to random position for random time
    /// </summary>
    /// <returns></returns>
    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));

        while (true)
        {
            targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }
}
