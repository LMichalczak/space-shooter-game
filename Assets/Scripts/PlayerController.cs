﻿using System;
using UnityEngine;
using UnityEngine.Playables;

[System.Serializable]
public class PositionXZLimits
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{

    private Rigidbody playerRigidbody;
    private AudioSource playerAudioSource;
    private float nextFire;

    public float speed;
    public float fireRate;
    public GameObject shot;
    public Transform shotSpawn;
    public PositionXZLimits positionXZLimits;
    public float tilt;

	void Start ()
	{
	    playerRigidbody = GetComponent<Rigidbody>();
	    playerAudioSource = GetComponent<AudioSource>();
	}

    void Update()
    {
        shotBolt();
    }

        void FixedUpdate ()
	{
	    movePlayer();
        limitPlayerPosition();
	    playerRigidbody.rotation = Quaternion.Euler(0.0f, 0.0f, playerRigidbody.velocity.x * -tilt);

	}

    /// <summary>
    /// Limit position of player controller on x and z axis
    /// </summary>
    void limitPlayerPosition()
    {
        playerRigidbody.position = new Vector3(
            Mathf.Clamp(playerRigidbody.position.x, positionXZLimits.xMin, positionXZLimits.xMax), 
            0.0f,
            Mathf.Clamp(playerRigidbody.position.z, positionXZLimits.zMin, positionXZLimits.zMax));
    }

    /// <summary>
    /// Add velocity to player using inputs
    /// </summary>
    void movePlayer()
    {
        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontalMovement, 0.0f, verticalMovement);
        playerRigidbody.velocity = movement * speed;
    }

    /// <summary>
    /// Shot a bolt in spawn position and rotation with shot rate 
    /// </summary>
    void shotBolt()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            playerAudioSource.Play();
        }
    }
   
}
