﻿using UnityEngine;

public class RandomRotator : MonoBehaviour
{
    private Rigidbody objectRigidvody;

    public float rotationValue;
	// Use this for initialization
	void Start ()
	{
	    objectRigidvody = GetComponent<Rigidbody>();
	    objectRigidvody.angularVelocity = Random.insideUnitSphere * rotationValue;
	}

}
