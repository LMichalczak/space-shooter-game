﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Vector3 spawnValues;
    public GameObject[] hazards;
    public GameObject colectableObject;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public Text scoreText;
    public Text gameOverText;
    public Text restartText;
    public Text fuelText;

    private int score;
    private float fuel;
    private bool gameOver;
    private bool restart;
    private PlayerController player;

    // Use this for initialization
    void Start ()
	{
	    restartText.text = "";
	    gameOverText.text = "";
        StartCoroutine(SpawnWaves());
	    score = 0;
	    fuel = 1000;
        UpdateScore();
	    GameObject playerGameObject = GameObject.FindWithTag("Player");
	    if (playerGameObject != null)
	        player = playerGameObject.GetComponent<PlayerController>();

    }

    // Update is called once per frame
    void Update()
    {
        checkFuel();
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("Main");
            }
        }
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);

        while (true)
        {
            int fuelPosition = Random.Range(0, hazardCount);
            for (int i = 0; i < hazardCount; i++)
            {
                SpawnObject(i, fuelPosition);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }



        

    void SpawnObject(int i, int fuelPosition)
    {
        GameObject objectToSpawn;
        if (i == fuelPosition)
        {
            objectToSpawn = colectableObject;
        }
        else
            objectToSpawn = hazards[Random.Range(0, hazards.Length)];

        Vector3 spawnPosition =
            new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
        Quaternion spawnRotation = Quaternion.identity;
        Instantiate(objectToSpawn, spawnPosition, spawnRotation);
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void AddScore(int scoreToAdd)
    {
        score += scoreToAdd;    
        UpdateScore();
    }

    void UpdateFuelText()
    {
        fuelText.text = "Fuel: " + fuel;
    }

    void checkFuel()
    {
        if (fuel > 0)
        {
            fuel--;
            UpdateFuelText();
            return;
        }
            GameOver();
            Destroy(player);
    }

    public void AddFuel(float fuelToAdd)
    {
        fuel += fuelToAdd;
        if (fuel > 1000)
            fuel = 1000;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
}
