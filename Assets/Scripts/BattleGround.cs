﻿using UnityEngine;

public class BattleGround : MonoBehaviour {
    void OnTriggerExit(Collider other)
    {
        // Destroy everything that leaves the trigger
        Destroy(other.gameObject);
    }
}
