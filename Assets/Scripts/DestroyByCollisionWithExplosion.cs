﻿using System.Collections;
using UnityEngine;

public class DestroyByCollisionWithExplosion : DestroyByCollision {
    public GameObject explosion;
    protected override bool OnTriggerEnter(Collider other)
    {   
        if (base.OnTriggerEnter(other)) 
            Instantiate(explosion, transform.position, transform.rotation);
        return true;
    }

}
