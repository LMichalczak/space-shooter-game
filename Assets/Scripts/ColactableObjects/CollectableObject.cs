﻿using System.Collections;
using UnityEngine;

public abstract class CollectableObject : MonoBehaviour
{
    private Rigidbody rb;
    protected GameController gameController;
    
    protected virtual void Start ()
    {
        rb = GetComponent<Rigidbody>();
        GameObject gameControllerGameObject = GameObject.FindWithTag("GameController");
        if (gameControllerGameObject != null)
            gameController = gameControllerGameObject.GetComponent<GameController>();
    }


}
