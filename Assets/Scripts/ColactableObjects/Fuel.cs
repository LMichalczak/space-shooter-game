﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuel : CollectableObject
{
    public float fuel;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameController.AddFuel(fuel);
            Destroy(gameObject);
        }
    }
}
